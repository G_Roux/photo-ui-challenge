import 'package:flutter/material.dart';
import 'package:photo_ui_challenge/presentation/my_flutter_app_icons.dart';
import 'package:photo_ui_challenge/utils/Theme.dart';

class ForgotPassPage extends StatelessWidget {
  final btnSize = 60.0;
  final bottomPadding = 20.0;

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: MyColors.scaffold,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              height: screenHeight - bottomPadding - btnSize / 2,
              width: screenWidth,
              decoration: BoxDecoration(
                color: MyColors.forgotPwd,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(40.0),
                  bottomRight: Radius.circular(40.0),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.arrow_back_ios),
                        color: Colors.white,
                        onPressed: () => Navigator.pop(context),
                      ),
                      SizedBox(width: 40.0),
                      Text(
                        "Forgot Password",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 22.0,
                        ),
                      )
                    ],
                  )),
            ),
            Positioned(
              top: screenHeight / 2 + 80.0,
              child: Container(
                width: screenWidth,
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Text(
                  "Enter your email and we will send you a link to reset your password",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey[300],
                    fontSize: 19.0,
                  ),
                ),
              ),
            ),
            Positioned(
                top: screenHeight / 2 - 40.0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Container(
                    padding: const EdgeInsets.all(20.0),
                    width: screenWidth - 32,
                    height: 80.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "Email",
                        icon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                )),
            Positioned(
              left: screenWidth / 2 - btnSize / 2,
              bottom: bottomPadding,
              child: Container(
                height: btnSize,
                width: btnSize,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  color: Colors.white,
                ),
                child: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: Icon(
                    MyFlutterApp.right_big,
                    color: MyColors.blueLogin,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
