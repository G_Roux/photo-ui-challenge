import 'package:flutter/material.dart';
import 'package:photo_ui_challenge/presentation/my_flutter_app_icons.dart';
import 'package:photo_ui_challenge/utils/Theme.dart';

class IntroPage extends StatelessWidget {
  final Function changePage;

  IntroPage(this.changePage);

  Widget _buildTitle() {
    return Text(
      "Flutter",
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 25.0,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    final btnSize = 60.0;
    final bottomPadding = 20.0;

    final imgHeight = screenHeight - 220.0;

    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: bottomPadding + btnSize / 2),
          child: Container(
            height: screenHeight,
            decoration: BoxDecoration(
              color: MyColors.blueLogin,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(40.0),
                bottomRight: Radius.circular(40.0),
              ),
            ),
          ),
        ),
        Container(
          height: imgHeight,
          width: screenWidth,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/mountain-day.png'),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(40.0),
              bottomRight: Radius.circular(40.0),
            ),
          ),
        ),
        Positioned(
          top: 70.0,
          left: screenWidth / 2 - 25,
          child: _buildTitle(),
        ),
        Positioned(
          top: imgHeight + bottomPadding,
          child: Container(
            padding: const EdgeInsets.all(16.0),
            width: screenWidth,
            child: Text(
              "Find and connect with the people who loves to travelling and enjoy nature and other photography",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 20.0),
            ),
          ),
        ),
        Positioned(
          left: screenWidth / 2 - btnSize / 2,
          bottom: bottomPadding,
          child: Container(
            height: btnSize,
            width: btnSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: Colors.white,
            ),
            child: IconButton(
              onPressed: () => changePage(),
              icon: Icon(
                MyFlutterApp.right_big,
                color: MyColors.blueLogin,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
