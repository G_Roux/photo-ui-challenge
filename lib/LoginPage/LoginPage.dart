import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_ui_challenge/LoginPage/FieldPage.dart';
import 'package:photo_ui_challenge/LoginPage/IntroPage.dart';
import 'package:photo_ui_challenge/utils/Theme.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final pageController = PageController(initialPage: 0);
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);

    List<Widget> pageChildren = [
      IntroPage(_changePage),
      FieldPage(),
    ];

    return Scaffold(
      backgroundColor: MyColors.scaffold,
      body: PageView(children: pageChildren, controller: pageController),
    );
  }

  _changePage() {
    setState(() {
      pageController.animateToPage(1,
          curve: Curves.easeIn, duration: Duration(milliseconds: 400));
    });
  }
}
