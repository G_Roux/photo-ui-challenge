import 'package:flutter/material.dart';
import 'package:photo_ui_challenge/ForgotPassPage/ForgotPassPage.dart';
import 'package:photo_ui_challenge/SignUpPage/SignUpPage.dart';
import 'package:photo_ui_challenge/presentation/my_flutter_app_icons.dart';
import 'package:photo_ui_challenge/utils/Theme.dart';

class FieldPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FieldPageState();
}

class _FieldPageState extends State<FieldPage> {
  final btnSize = 60.0;
  final bottomPadding = 20.0;

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Stack(
      children: <Widget>[
        Container(
          height: screenHeight - bottomPadding - btnSize / 2,
          width: screenWidth,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/landscape-night.png'),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(40.0),
              bottomRight: Radius.circular(40.0),
            ),
          ),
        ),
        Positioned(
          top: bottomPadding,
          right: bottomPadding,
          child: Container(
            alignment: Alignment.center,
            width: 40.0,
            height: 40.0,
            decoration: BoxDecoration(
              color: MyColors.darkBtn,
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => SignUpPage())),
            ),
          ),
        ),
        Positioned(
          left: 16,
          bottom: 120.0,
          child: _buildInputField(screenWidth),
        ),
        Positioned(
          left: btnSize / 2,
          bottom: bottomPadding,
          child: _buildBtnRow(screenWidth),
        ),
      ],
    );
  }

  Widget _buildBtnRow(double screenWidth) {
    return Container(
      width: screenWidth - btnSize,
      child: Row(
        children: <Widget>[
          Container(
            height: btnSize,
            width: btnSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: MyColors.facebookBtn,
            ),
            child: IconButton(
              icon: Icon(
                MyFlutterApp.facebook,
                color: Colors.white,
              ),
              onPressed: () {},
            ),
          ),
          SizedBox(width: 10.0),
          Container(
            height: btnSize,
            width: btnSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: MyColors.twitterBtn,
            ),
            child: IconButton(
              icon: Icon(
                MyFlutterApp.twitter,
                color: Colors.white,
              ),
              onPressed: () {},
            ),
          ),
          Expanded(child: Container()),
          Container(
            height: btnSize,
            width: btnSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: Colors.white,
            ),
            child: IconButton(
              icon: Icon(
                MyFlutterApp.right_big,
                color: MyColors.blueLogin,
              ),
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildInputField(double screenWidth) {
    return Container(
      width: screenWidth - 32,
      height: 195.0,
      padding: const EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
                border: InputBorder.none,
                icon: Icon(Icons.email),
                hintText: "Email or Username",
                hintStyle: TextStyle(color: Colors.grey)),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Container(
              width: double.maxFinite,
              height: 1,
              color: Colors.grey[200],
            ),
          ),
          TextField(
            decoration: InputDecoration(
                border: InputBorder.none,
                icon: Icon(Icons.lock),
                hintText: "Password",
                hintStyle: TextStyle(color: Colors.grey)),
          ),
          FlatButton(
              child: Text(
                "Forgot password ?",
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext contexy) => ForgotPassPage(),
                    ),
                  ))
        ],
      ),
    );
  }
}
