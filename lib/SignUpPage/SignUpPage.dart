import 'package:flutter/material.dart';
import 'package:photo_ui_challenge/presentation/my_flutter_app_icons.dart';
import 'package:photo_ui_challenge/utils/Theme.dart';

class SignUpPage extends StatelessWidget {
  final btnSize = 60.0;
  final bottomPadding = 20.0;

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: MyColors.scaffold,
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Container(
                height: screenHeight - bottomPadding - btnSize / 2,
                width: screenWidth,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/landscape-night.png'),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40.0),
                    bottomRight: Radius.circular(40.0),
                  ),
                ),
              ),
              Align(
                  alignment: Alignment.topLeft,
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(10.0),
                        child: IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          color: Colors.white,
                          onPressed: () => Navigator.pop(context),
                        ),
                      ),
                      SizedBox(width: 40.0),
                      Text(
                        "Sign up",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 22.0,
                        ),
                      )
                    ],
                  )),
              Positioned(
                left: 16,
                bottom: 120.0,
                child: _buildInputField(screenWidth),
              ),
              Positioned(
                left: btnSize / 2,
                bottom: bottomPadding,
                child: _buildBtnRow(screenWidth),
              ),
            ],
          ),
        ));
  }

  _buildInputField(double screenWidth) {
    return Container(
      width: screenWidth - 32,
      height: 211.0,
      padding: const EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
                border: InputBorder.none,
                icon: Icon(Icons.person),
                hintText: "Name",
                hintStyle: TextStyle(color: Colors.grey)),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Container(
              width: double.maxFinite,
              height: 1,
              color: Colors.grey[200],
            ),
          ),
          TextField(
            decoration: InputDecoration(
                border: InputBorder.none,
                icon: Icon(Icons.email),
                hintText: "Email",
                hintStyle: TextStyle(color: Colors.grey)),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Container(
              width: double.maxFinite,
              height: 1,
              color: Colors.grey[200],
            ),
          ),
          TextField(
            decoration: InputDecoration(
                border: InputBorder.none,
                icon: Icon(Icons.lock),
                hintText: "Password",
                hintStyle: TextStyle(color: Colors.grey)),
          ),
        ],
      ),
    );
  }

  _buildBtnRow(double screenWidth) {
    return Container(
      width: screenWidth - btnSize,
      child: Row(
        children: <Widget>[
          Container(
            height: btnSize,
            width: btnSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: MyColors.facebookBtn,
            ),
            child: IconButton(
              icon: Icon(
                MyFlutterApp.facebook,
                color: Colors.white,
              ),
              onPressed: () {},
            ),
          ),
          SizedBox(width: 10.0),
          Container(
            height: btnSize,
            width: btnSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: MyColors.twitterBtn,
            ),
            child: IconButton(
              icon: Icon(
                MyFlutterApp.twitter,
                color: Colors.white,
              ),
              onPressed: () {},
            ),
          ),
          Expanded(child: Container()),
          Container(
            height: btnSize,
            width: btnSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: Colors.white,
            ),
            child: IconButton(
              icon: Icon(
                MyFlutterApp.right_big,
                color: MyColors.blueLogin,
              ),
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}
