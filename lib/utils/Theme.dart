import 'package:flutter/material.dart';

class MyColors {
  static Color scaffold = Color(0xffe9eef2);
  static Color facebookBtn = Color(0xff6588f5);
  static Color twitterBtn = Color(0xff1dcaff);
  static Color blueLogin = Color(0xff3fa1ff);
  static Color darkBtn = Color(0xff354656);
  static Color forgotPwd = Color(0xff2c3a48);
}
