# <img src="https://gitlab.com/G_Roux/photo-ui-challenge/raw/master/assets/ic_launcher.png" height="60"> Presentation

This is a personal project to challenge myself in implementing design in Flutter.

## Screenshots

<img src="flutter_02.png" alt="drawing" height="500"/>
<img src="flutter_01.png" alt="drawing" height="500"/>
<img src="flutter_03.png" alt="drawing" height="500"/>
<img src="flutter_04.png" alt="drawing" height="500"/>

## Credits

### Designs

* [Login Screen](https://dribbble.com/shots/2822759-Flutter-intro-screens-photography-app)
* [SignUp Screen](https://dribbble.com/shots/2838853-Sign-Up-Steps-for-flutter)
* [Forgot Password Screen](https://dribbble.com/shots/2969901-Trending-forgot-password-flutter-app)

Those designs were made by [Prakhar Neel Sharma](https://dribbble.com/prakhar)

### Images

* https://www.vecteezy.com/vector-art/203030-vector-forest-landscape-illustration
* https://www.vecteezy.com/vector-art/203029-vector-sunset-landscape-illustration

